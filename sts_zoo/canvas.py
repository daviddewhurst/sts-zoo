# copyright David Rushing Dewhurst, 2022 - present

import matplotlib.pyplot as plt
import numpy as np

from .animals import ModelFactory


def plot_factory(factory: ModelFactory, savedir, num_subplots=2,):
    """
    Plots example draws from a `ModelFactory`. Intended to help users get a heuristic
    understanding of what the DGPs "look like". Will save a `png` image in `savedir`.
    
    *Args:*

    + `factory (ModelFactory)`: an instance of a subclass of `ModelFactory`; will be used
        to generate draws
    + `savedir`: some `pathlib.Path`-like object, directory at which to save plot
    + `num_subplots (int)`: the number of distinct draws (and hence subplots) to draw
        from the DGP
    """
    fig, axes = plt.subplots(num_subplots, 1)
    model = factory()
    time = np.arange(factory.t0, factory.t1)
    factory_name = f"{type(factory).__name__}"

    for ax in axes:
        values = model()
        for ix in range(factory.size):
            ax.plot(time, values[ix])
        ax.set_xlabel("Time (dimensionless)")
        ax.set_ylabel(factory_name)

    plt.tight_layout()
    plt.savefig(savedir / (factory_name + "-draws.png"))
    plt.close()
