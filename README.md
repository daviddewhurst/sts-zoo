# `sts-zoo`

Exotic structural time series animals.

## Installation

+ `conda create --name my-sts-env python==3.7`
+ `conda activate my-sts-env`
+ `/opt/anaconda3/envs/my-sts-env/bin/python -m pip install -r requirements.txt`