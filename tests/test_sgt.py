# copyright David Rushing Dewhurst, 2022 - present

import logging
import pathlib

import pyro
import pyro.distributions as dist

from sts_zoo.animals import SGTFactory
from sts_zoo.canvas import plot_factory


OUTPATH = pathlib.Path("./out")
OUTPATH.mkdir(exist_ok=True, parents=True,)


def test_sgt_basic():
    size = 3
    t0 = 1
    t1 = 52
    num_seasons = 7

    sgt_factory = SGTFactory(
        t0=t0,
        t1=t1,
        size=size,
        num_seasons=num_seasons,
        beta=dist.Normal(0.0, 2.0 / (t1 - t0)).expand((size,)).to_event(1),
        seasons=dist.Normal(0.0, 5.0).expand((size, num_seasons)).to_event(2),
    )
    sgt_model_1 = sgt_factory()
    sgt_model_2 = sgt_factory()

    logging.info(f"Created a sgt factory: {sgt_factory}")
    logging.info(f"Created a sgt model: {sgt_model_1}")
    logging.info(f"And another one: {sgt_model_2}")

    sgt1_trace = pyro.poutine.trace(sgt_model_1).get_trace()
    sgt1_trace.log_prob_sum()
    logging.info(f"Traced sgt 1:\n{sgt1_trace.nodes}")
    sgt2_trace = pyro.poutine.trace(sgt_model_2).get_trace()
    sgt2_trace.log_prob_sum()
    logging.info(f"Traced sgt 2:\n{sgt2_trace.nodes}")
    for (k1, k2) in zip(sgt1_trace.nodes.keys(), sgt2_trace.nodes.keys()):
        if  (k1 != "_INPUT") and (k1 != "_RETURN"):
            assert k1 != k2

    plot_factory(sgt_factory, OUTPATH)
    