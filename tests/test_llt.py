# copyright David Rushing Dewhurst, 2022 - present

import logging
import pathlib

import pyro
import pyro.distributions as dist

from sts_zoo.animals import LLTFactory
from sts_zoo.canvas import plot_factory


OUTPATH = pathlib.Path("./out")
OUTPATH.mkdir(exist_ok=True, parents=True,)


def test_llt_basic():
    size = 2
    t0 = 1
    t1 = 52

    llt_factory = LLTFactory(t0=t0, t1=t1, size=size,)
    llt_model_1 = llt_factory()
    llt_model_2 = llt_factory()

    logging.info(f"Created an llt factory: {llt_factory}")
    logging.info(f"Created an llt model: {llt_model_1}")
    logging.info(f"And another one: {llt_model_2}")

    llt1_trace = pyro.poutine.trace(llt_model_1).get_trace()
    llt1_trace.log_prob_sum()
    logging.info(f"Traced llt 1:\n{llt1_trace.nodes}")
    llt2_trace = pyro.poutine.trace(llt_model_2).get_trace()
    llt2_trace.log_prob_sum()
    logging.info(f"Traced llt 2:\n{llt2_trace.nodes}")
    for (k1, k2) in zip(llt1_trace.nodes.keys(), llt2_trace.nodes.keys()):
        if  (k1 != "_INPUT") and (k1 != "_RETURN"):
            assert k1 != k2

    plot_factory(llt_factory, OUTPATH)
    