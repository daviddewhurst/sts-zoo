# copyright David Rushing Dewhurst, 2022 - present

import logging
import pathlib

import pyro
import pyro.distributions as dist
from stsb3 import sts
import torch

from sts_zoo.animals import StochVolFactory
from sts_zoo.canvas import plot_factory


OUTPATH = pathlib.Path("./out")
OUTPATH.mkdir(exist_ok=True, parents=True,)


def test_stochvol_basic():
    size = 3
    t0 = 1
    t1 = 52
    dt = torch.tensor(1.0 / (t1 - t0))

    vol_factory = StochVolFactory(
        t0=t0,
        t1=t1,
        size=size,
        ic_ispvol=torch.tensor(1e-1),
    )
    vol_model_1 = vol_factory()
    vol_model_2 = vol_factory()

    logging.info(f"Created a vol factory: {vol_factory}")
    logging.info(f"Created a vol model: {vol_model_1}")
    logging.info(f"And another one: {vol_model_2}")

    vol1_trace = pyro.poutine.trace(vol_model_1).get_trace()
    vol1_trace.log_prob_sum()
    logging.info(f"Traced vol 1:\n{vol1_trace.nodes}")
    vol2_trace = pyro.poutine.trace(vol_model_2).get_trace()
    vol2_trace.log_prob_sum()
    logging.info(f"Traced vol 2:\n{vol2_trace.nodes}")
    for (k1, k2) in zip(vol1_trace.nodes.keys(), vol2_trace.nodes.keys()):
        if  (k1 != "_INPUT") and (k1 != "_RETURN"):
            assert k1 != k2

    plot_factory(vol_factory, OUTPATH)
    
    # demonstrate training, posterior inference, and forecasting
    data = vol_model_1()
    lf = sts.GaussianNoise(
        vol_model_1,
        t0=t0,
        t1=t1,
        size=size,
        data=data,
        scale=torch.tensor(0.01)
    )
    logging.info(f"Created likelihood function: {lf}")
    lf.fit(
        method="advi",
        method_kwargs={
            "loss": "TraceGraph_ELBO"
        },
        verbosity=1e-2,
    )
    nsamples = 10
    pp = lf.posterior_predictive(nsamples=nsamples)
    logging.info(f"Posterior predictive draws:\n{pp}")
    fcst = sts.forecast(vol_model_1, pp, Nt=5, nsamples=10,)
    logging.info(f"Created forecast:\n{fcst}")
