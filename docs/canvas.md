# `sts_zoo.canvas`

## `ModelFactory`
Abstract base class for all model factories. Ensures unique ids for each
component of each generated model, and encapsulates model generation through
`make` method.

### `make`
Abstract method to be overridden. Calling `.make()` should
create a new `stsb3` model with guaranteed unique site names.



## `plot_factory`
Plots example draws from a `ModelFactory`. Intended to help users get a heuristic
understanding of what the DGPs "look like". Will save a `png` image in `savedir`.

*Args:*

+ `factory (ModelFactory)`: an instance of a subclass of `ModelFactory`; will be used
    to generate draws
+ `savedir`: some `pathlib.Path`-like object, directory at which to save plot
+ `num_subplots (int)`: the number of distinct draws (and hence subplots) to draw
    from the DGP


