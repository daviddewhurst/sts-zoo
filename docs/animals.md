# `sts_zoo.animals`

## `LLTFactory`
A local linear trend (LLT) factory.

The generative model for programs created by this factory is

\begin{aligned}
l_t &= l_{t - 1} + \mathrm{loc}_t + \mathrm{scale\_local}_t w^l_t,\ l_0 = \mathrm{ic\_local} \\
z_t &= z_{t - 1} + l_t + \mathrm{scale\_level}_t w^z_t,\ z_0 = \mathrm{ic\_level},
\end{aligned}

where $w^\cdot_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$.

*Args:*

+ `loc (Block || torch.tensor || pyro.distributions)`: location parameter
+ `scale_local (Block || torch.tensor || pyro.distributions)`: scale parameter for $l_t$
+ `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
+ `ic_local (torch.tensor || pyro.distributions)`: initial condition for $l_t$
+ `ic_level (torch.tensor || pyro.distributions)`: initial condition for $z_t$

### `make`
Creates a new LLT model. The sites in the created model are guaranteed
to be unique.

*Args:*

None



## `ModelFactory`
Abstract base class for all model factories. Ensures unique ids for each
component of each generated model, and encapsulates model generation through
`make` method.

### `make`
Abstract method to be overridden. Calling `.make()` should
create a new `stsb3` model with guaranteed unique site names.



## `SGTFactory`
A seasonal + global trend (SGT) factory.

The generative model for programs created by this factory is

\begin{aligned}
l_t &= \alpha_t + \beta_t l_{t-1} + \mathrm{scale\_local}_t w_t \\
g_t &= a + bt \\
s_t &= \theta_{t \mathrm{mod} s},\ s=1,...,\mathrm{num\_seasons}\\
z_t &= s_t + g_t + l_t,
\end{aligned}

where $w_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$,
and $\mathrm{num\_seasons}$ is the number of discrete seasons.

*Args:*

+ `alpha (Block || torch.tensor || pyro.distributions)`: intercept parameter of $l_t$
+ `beta (Block || torch.tensor || pyro.distributions)`: slope parameter of $l_t$
+ `a (Block || torch.tensor || pyro.distributions)` intercept parameter of $g_t$
+ `b (Block || torch.tensor || pyro.distributions)` slope parameter of $g_t$
+ `num_seasons (int)`: $\geq 2$ integer number of seasons, defaults to 2
+ `seasons (torch.tensor || pyro.distributions)` season values
+ `scale_local (Block || torch.tensor || pyro.distributions)`: scale parameter for $l_t$
+ `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
+ `ic_level (torch.tensor || pyro.distributions)`: initial condition for $z_t$

### `make`
Creates a new SGT model. The sites in the created model are guaranteed
to be unique.

*Args:*

None



## `SLLTFactory`
A semi-local linear trend (SLLT) factory.

The generative model for programs created by this factory is

\begin{aligned}
l_t &= \alpha_t + \beta_t l_{t-1} + \mathrm{scale\_local}_t w^l_t \\
z_t &= z_{t - 1} + l_t + \mathrm{scale\_level}_t w^z_t,\ z_0 = \mathrm{ic\_level},
\end{aligned}

where $w^\cdot_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$.

*Args:*

+ `alpha (Block || torch.tensor || pyro.distributions)`: intercept parameter of $l_t$
+ `beta (Block || torch.tensor || pyro.distributions)`: slope parameter of $l_t$
+ `scale_local (Block || torch.tensor || pyro.distributions)`: scale parameter for $l_t$
+ `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
+ `ic_level (torch.tensor || pyro.distributions)`: initial condition for $z_t$

### `make`
Creates a new SLLT model. The sites in the created model are guaranteed
to be unique.

*Args:*

None



## `StochVolFactory`
A stochastic volatility model factory.

This is a model for the log of an asset price $S_t$. Define $z_t = \log S_t$.
The generative model for programs created by this factory is

\begin{aligned}
\mathrm{isp}\ \sigma_t &= \mathrm{isp}\ \sigma_{t - 1} + \mathrm{d}t_t\ \mathrm{loc\_ispvol}_t\ +
     \sqrt{\mathrm{d}t_t}\ \mathrm{scale\_ispvol}_t\ w^{\mathrm{isp}\sigma}_t,\ l_0 = \mathrm{ic\_logvol} \\
z_t &= z_{t - 1} + \mathrm{d}t_t\mathrm{loc}_t + \sqrt{\mathrm{d}t_t}\sigma_t w^z_t,\ z_0 = \mathrm{ic},
\end{aligned}

where $w^\cdot_t \sim \mathrm{Normal}(0, 1)$, $t_0 \leq t \leq t_1$, and $\mathrm{isp}$ is the inverse softplus function,
$\mathrm{isp}(y) = \log(-1 + \exp y)$. 

*Args:*

+ `dt (Block || torch.tensor || pyro.distributions)`: time discretization. If a `pyro.distributions` object, it should
    be an atomless distribution with non-negative support and will be interpreted as
    $p(dt) = \prod_{t_0\leq t\leq t_1} p(dt_t)$, not as a single draw. Passing a distributions object can be a useful method for modeling
    jump-y asset prices (e.g., $p(dt_t) = \mathrm{Gamma}(dt_t | \alpha, \beta)$ generates the variance gamma process).
    However, doing this also makes the model non-identifiable in the scale parameter(s).
+ `loc_ispvol (Block || torch.tensor || pyro.distributions)`: location parameter of the isp volatility process
+ `vol_ispvol (Block || torch.tensor || pyro.distributions)`: scale/volatility parameter of the isp volatility process
+ `ic_ispvol (torch.tensor || pyro.distributions)`: initial condition of the isp_volatility process
+ `scale_level (Block || torch.tensor || pyro.distributions)`: scale parameter for $z_t$
+ `loc (Block || torch.tensor || pyro.distributions)`: location parameter for the log asset price process.
+ `ic (torch.tensor || pyro.distributions)`: initial condition for the log asset price process

### `make`
Creates a new StochVol model. The sites in the created model are guaranteed
to be unique.

*Args:*

None



